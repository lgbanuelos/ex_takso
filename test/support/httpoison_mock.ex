defmodule Takso.HTTPoisonMock do
    def get!(url) do
        %{query: query} = URI.parse(url)
        %{"origins" => origins, "destinations" => destinations} = URI.decode_query(query)
        origins = process_query(origins)
        destinations = process_query(destinations)

        cond do
        length(origins) == 1 and length(destinations) == 1 ->
            %HTTPoison.Response{
                body: File.read!("test/fixtures/#{origins}__#{destinations}.json")
            }
        length(origins) > 1 and length(destinations) == 1 ->
            %HTTPoison.Response{
                body: File.read!("test/fixtures/#{destinations}.json")
            }
        true ->
            %HTTPoison.Response{
                body: """
                {
                    "destination_addresses" : [ "" ],
                    "origin_addresses" : [ "" ],
                    "rows" : [{ "elements" : [{ "status" : "NOT_FOUND" }] }],
                    "status" : "OK"
                 }
                      """
            }
        end
    end

    defp process_query(str) do
        cond do
            String.contains?(str, "|") ->
                String.split(str, "|")
                |> Enum.map(fn loc -> process_location(loc) end)
            true ->
                [process_location(str)]
        end
    end

    defp process_location(loc) do
        String.replace(loc, "Tartu", "")
        |> String.replace("Estonia", "")
        |> String.trim
        |> String.replace(" ", "_")
    end
end