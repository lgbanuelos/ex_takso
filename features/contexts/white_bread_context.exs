defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers
  alias Takso.{Repo,Taxi, User}
  
  @headless_chrome [additional_capabilities: %{
      chromeOptions: %{ "args" => [
          "--user-agent=#{Hound.Browser.user_agent(:chrome)}",
          "--headless",
          "--no-sandbox",
          "--disable-gpu"
      ]}
    }]

  scenario_timeouts fn _feature, _scenario ->
    300_000
  end
  
  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)    
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session(@headless_chrome) # Sets up headless chrome for default session
    Ecto.Adapters.SQL.Sandbox.checkout(Takso.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(Takso.Repo, {:shared, self()})
    %{}
  end
  scenario_finalize fn _status, _state ->
    Ecto.Adapters.SQL.Sandbox.checkin(Takso.Repo)
    Hound.end_session
  end
  
  def login("customer", password) do
    change_to_default_session()
    navigate_to "/#/login"
    Process.sleep(200)
    fill_field({:id, "username"}, "customer")
    fill_field({:id, "password"}, password)
    click({:id, "submit"})
    IO.puts "Customer logged in"
  end
  def login(username, password) do
    change_session_to String.to_atom(username), @headless_chrome
    navigate_to "/#/login"
    Process.sleep(200)    
    fill_field({:id, "username"}, username)
    fill_field({:id, "password"}, password)
    click({:id, "submit"})
    IO.puts "Driver logged in"    
  end
  
  given_ ~r/^the following taxis are on duty$/, 
  fn state, %{table_data: table} ->
    table
    |> Enum.map(fn taxi -> Taxi.changeset(%Taxi{}, taxi) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

    # Insert to the database a new user for each taxi driver

    table
    |> Enum.map(fn taxi -> User.changeset(%User{}, %{name: taxi.username, username: taxi.username, password: "parool", role: "taxi-driver"}) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

    changeset = User.changeset(%User{}, %{name: "customer", username: "customer", password: "parool", role: "customer"})
    Repo.insert!(changeset)

    login("customer", "parool")    
    table
    |> Enum.each(fn taxi -> login(taxi.username, "parool") end)

    {:ok, state}
  end

  and_ ~r/^the status of the taxis is "(?<statuses>[^"]+)"$/,
  fn state, %{statuses: statuses} ->
    String.split(statuses, ",")
    |> Enum.with_index
    |> Enum.map(fn {status, index} -> 
          Repo.get_by!(Taxi, username: "taxi#{index + 1}")
          |> Taxi.changeset(%{status: status})
          |> Repo.update!
        end)
    {:ok, state}
  end

  and_ ~r/^I want to go from "(?<pickup_address>[^"]+)" to "(?<dropoff_address>[^"]+)"$/,
  fn state, %{pickup_address: pickup_address, dropoff_address: dropoff_address} ->
    {:ok, state |> Map.put(:pickup_address, pickup_address) |> Map.put(:dropoff_address, dropoff_address)}
  end

  and_ ~r/^I enter the booking information on the STRS Customer app$/, fn state ->
    change_to_default_session()
    fill_field({:id, "pickup_address"}, state.pickup_address)
    fill_field({:id, "dropoff_address"}, state.dropoff_address)
    {:ok, state}
  end

  when_ ~r/^I summit the booking request$/, fn state ->
    click({:id, "submit"})
    {:ok, state}
  end

  and_ ~r/^"(?<taxi_username>[^"]+)" is contacted$/,
  fn state, %{taxi_username: _taxi_username} ->
    {:ok, state}
  end

  and_ ~r/^"(?<taxi_username>[^"]+)" decides to "(?<decision>[^"]+)"$/,
  fn state, %{taxi_username: taxi_username, decision: decision} ->
    change_session_to String.to_atom(taxi_username), @headless_chrome
    Process.sleep(1000)
    case decision do
      "reject" ->
        click({:id, "rejectbtn"})
      "accept" ->
        click({:id, "acceptbtn"})
      _ -> 
        Process.sleep(1500)
    end
    {:ok, state}
  end

  then_ ~r/^I should be notified "(?<notification>[^"]+)"$/,
  fn state, %{notification: notification} ->
    change_to_default_session()
    Process.sleep(500)
    assert visible_in_page? Regex.compile!(notification)
    {:ok, state}
  end
end
