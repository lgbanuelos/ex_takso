defmodule Takso.BookingAPIController do
  use Takso.Web, :controller
  alias Takso.{Repo,Booking}
  
  def create(conn, params) do
    user = Guardian.Plug.current_resource(conn)

    changeset = Booking.changeset(%Booking{}, params)
    booking = Repo.insert!(changeset)

    booking_reference = String.to_atom("booking_#{booking.id}")
    Takso.TaxiAllocator.start_link(params |> Map.put(:customer_username, user.username) |> Map.put(:booking_id, booking.id), booking_reference)
    Takso.TaxiAllocator.find_taxi(String.to_atom("booking_#{booking.id}"))

    conn
    |> put_status(201)
    |> json(%{msg: "We are processing your request"})
  end

  def update(conn, %{"id" => booking_id, "status" => decision}) do
    user = Guardian.Plug.current_resource(conn)    
    case decision do
      "rejected" ->
        Takso.TaxiAllocator.reject_booking(String.to_atom("booking_#{booking_id}"), user.username)
      "accepted" ->
        Takso.TaxiAllocator.accept_booking(String.to_atom("booking_#{booking_id}"), user.username)
    end

    conn
    |> put_status(200)
    |> json(%{msg: "Your decision has been taken into account"})
  end
end